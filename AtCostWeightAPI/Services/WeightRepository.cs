﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtCostWeightAPI.Models;
using System.IO.Ports;
using System.Configuration;
using System.Threading;
using System.IO;
using log4net;
using System.Runtime.Caching;

namespace AtCostWeightAPI.Services
{
    public class WeightRepository
    {
        public SerialPort _serialPort = new SerialPort();
        ObjectCache _cache = MemoryCache.Default;
        private string[] ExistingPorts = SerialPort.GetPortNames();
        public bool _continue = true;
        public string wt = "0.0";
        private static log4net.ILog Log { get; set; }
        ILog log = log4net.LogManager.GetLogger(typeof(WeightRepository));
        private string _portName = string.Empty;
        private string _weightScaleType = string.Empty;
        //private string _weightScaleType = ConfigurationManager.AppSettings["WEIGHTSCALETYPE"].ToString();

        public Weight GetWeight()
        {
            _portName = _cache["portname"] as string;
            _weightScaleType = _cache["scaletype"] as string;

            if (string.IsNullOrEmpty(_portName))
                _portName = ConfigurationManager.AppSettings["PORTNAME"].ToString();

            if (string.IsNullOrEmpty(_weightScaleType))
                _weightScaleType = ConfigurationManager.AppSettings["WEIGHTSCALETYPE"].ToString();

            return new Weight
            {
                Success = true,
                Value = 23.409,
                _errorMessage = null
            };
        }

        public Weight ErrorTest()
        {
            return new Weight
            {
                Success = false,
                Value = 0.000,
                _errorMessage = new Error("Sorry the Weight could not be calculated!")
            };
        }

        public Weight GetCurrentWeight()
        {
            _portName = _cache["portname"] as string;
            _weightScaleType = _cache["scaletype"] as string;

            if (string.IsNullOrEmpty(_portName))
                _portName = ConfigurationManager.AppSettings["PORTNAME"].ToString();

            if (string.IsNullOrEmpty(_weightScaleType))
                _weightScaleType = ConfigurationManager.AppSettings["WEIGHTSCALETYPE"].ToString();

            var weight = new Weight();
            var data = 0.0;
            try
            {
                // Create a new SerialPort object with default settings.
                // Allow the user to set the appropriate properties.
                
                var scaleType = !string.IsNullOrEmpty(_weightScaleType) ? _weightScaleType : throw new Exception("Weight scale type not configured properly. Please set the WEIGHTSCALETYPE property in web.config.");
                _serialPort.PortName = !string.IsNullOrEmpty(_portName) ? _portName : throw new Exception("Port name not Configured properly. Please set the PORTTYPE property in web.config.");
                _serialPort.BaudRate = 9600;
                _serialPort.Parity = Parity.None;
                _serialPort.DataBits = 8;
                _serialPort.StopBits = StopBits.One;
                _serialPort.DtrEnable = true;
                _serialPort.RtsEnable = true;
                _serialPort.Handshake = Handshake.RequestToSendXOnXOff;
                _serialPort.Encoding = System.Text.Encoding.ASCII;
                
                _serialPort.ReadTimeout = 500;
                _serialPort.WriteTimeout = 500;
                
                if (!ExistingPorts.Contains(_portName))
                    throw new Exception(string.Format("{0} port is not present. Please configure {0} port in device manager before using the api.", _portName));

                if (_serialPort.IsOpen)
                {
                    log.Debug("Serial Port is Open");
                    _serialPort.Close();
                }
                    
                if (!_serialPort.IsOpen)
                {
                    log.Debug("Serial Port is Closed");
                    _serialPort.Open();
                }


                _serialPort.DataReceived += new SerialDataReceivedEventHandler(_serialPort_DataReceived);

                try
                {
                    data = ReadContent(scaleType);
                    log.Debug("The Value of Data: " + data.ToString());
                    weight._errorMessage = null;
                    weight.Success = true;
                    weight.Value = data;
                }
                catch (Exception ex)
                {
                    weight._errorMessage = new Error(ex.Message.ToString() + " Inner Exception Weight: " + wt);
                    weight.Success = false;
                    weight.Value = 0.0;
                    //weight.Value = "0.0";
                    _serialPort.Close();
                    _continue = false;
                    log.Error(ex);
                }
                //_serialPort.Close();
            }
            catch (Exception ex)
            {
                weight._errorMessage = new Error(ex.Message.ToString());
                weight.Success = false;
                weight.Value = 0.0;
                weight.names = ExistingPorts;
                _serialPort.Close();
                _continue = false;
                log.Error(ex);
                //weight.Value = "0.0";
            }

            return weight;
        }

        public Weight GetCurrentWeightTest(string portName)
        {
            var weight = new Weight();
            var data = 0.0;
            try
            {
                // Create a new SerialPort object with default settings.
                // Allow the user to set the appropriate properties.

                var scaleType = !string.IsNullOrEmpty(_weightScaleType) ? _weightScaleType : ConfigurationManager.AppSettings["WEIGHTSCALETYPE"].ToString();
                _serialPort.PortName = portName;
                _serialPort.BaudRate = 9600;
                _serialPort.Parity = Parity.None;
                _serialPort.DataBits = 8;
                _serialPort.StopBits = StopBits.One;
                _serialPort.DtrEnable = true;
                _serialPort.RtsEnable = true;
                _serialPort.Handshake = Handshake.RequestToSendXOnXOff;
                _serialPort.Encoding = System.Text.Encoding.ASCII;

                _serialPort.ReadTimeout = 500;
                _serialPort.WriteTimeout = 500;

                if (!ExistingPorts.Contains(_serialPort.PortName))
                    throw new Exception(string.Format("{0} port is not present. Please configure {0} port in device manager before using the api.", _serialPort.PortName));

                if (_serialPort.IsOpen)
                {
                    log.Debug("Serial Port is Open");
                    _serialPort.Close();
                }

                if (!_serialPort.IsOpen)
                {
                    log.Debug("Serial Port is Closed");
                    _serialPort.Open();
                }

                //log.InfoFormat("Registering Weight handler with port: {0}", _serialPort.PortName);
                _serialPort.DataReceived += new SerialDataReceivedEventHandler(_serialPort_DataReceived);

                try
                {
                    //log.InfoFormat("Calling ReadContent method with port: {0}", _serialPort.PortName);
                    data = ReadContent(scaleType);
                    log.Debug("The Value of Data: " + data.ToString());
                    weight._errorMessage = null;
                    weight.Success = true;
                    weight.Value = data;
                }
                catch (Exception ex)
                {
                    weight._errorMessage = new Error(ex.Message.ToString() + " Inner Exception Weight: " + wt);
                    weight.Success = false;
                    weight.Value = 0.0;
                    //weight.Value = "0.0";
                    _serialPort.Close();
                    _continue = false;
                    log.Error(ex);
                }
                //_serialPort.Close();
            }
            catch (Exception ex)
            {
                weight._errorMessage = new Error(ex.Message.ToString());
                weight.Success = false;
                weight.Value = 0.0;
                weight.names = ExistingPorts;
                _serialPort.Close();
                _continue = false;
                log.Error(ex);
                //weight.Value = "0.0";
            }

            return weight;
        }

        private double ReadContent(string scaleType)
        {
            var stopTime = DateTime.Now.AddSeconds(5.0);
            while (_continue)
            {
                if (DateTime.Now >= stopTime)
                {
                    wt = "0.0";
                    break;
                }
            }
            var returnVal = 0.0;
            try
            {
                //log.InfoFormat("The value of Weight: {0}", wt);
                wt = new string(wt.Where(c => !char.IsControl(c) && !char.IsLetter(c) && !char.IsPunctuation(c) && !char.IsWhiteSpace(c) && !char.IsSeparator(c)).ToArray());           
                //log.InfoFormat("The value of Weight after replacing: {0}", wt);
                if (wt.Length > 6)
                    wt = wt.Substring(0, 6);
                
                //log.InfoFormat("The value of Weight after replacing and substring: {0}", wt);
                returnVal = Math.Round(Convert.ToDouble(wt), 3);

                if (scaleType.ToLower() == "g")
                {
                    returnVal = Math.Round(returnVal / 1000, 3);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
            return returnVal;
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(1000);
            log.Debug(e);
            try
            {
                //byte[] buffer = null;
                //while (null == buffer) // Read 20 bytes from //
                //{
                //    _serialPort.Read(buffer, 0, 20);
                //    log.InfoFormat("Current 20 bytes from buffer: {0}", Convert.ToInt32(buffer[0]));
                //}

                //wt = _serialPort.ReadExisting().ToString();
                //wt = Convert.ToDouble(_serialPort.ReadByte()).ToString();
                //log.InfoFormat("Configured Port Name: {0}", _serialPort.PortName);
                //log.InfoFormat("Number of bytes of data in the recieve buffer: {0}", _serialPort.BytesToRead);
                while (_serialPort.BytesToRead > 0)
                {
                    //log.InfoFormat("Current State of data in the buffer in different formats");
                    //log.InfoFormat("ReadExisting output: {0}", _serialPort.ReadExisting());
                    //log.InfoFormat("ReadByte output: {0}", _serialPort.ReadByte());
                    //log.InfoFormat("ReadChar output: {0}", _serialPort.ReadChar());
                    log.InfoFormat("ReadLine output: {0}", _serialPort.ReadLine());

                    wt = _serialPort.ReadLine().ToString();
                    //log.InfoFormat("Value of wt variable: {0}", wt);
                    //var existingValue = _serialPort.ReadExisting();
                    wt = Convert.ToDouble(wt).ToString();
                    //log.InfoFormat("Value of wt variable after double parsing: {0}", wt);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            log.Debug("The weight after handler called: " + wt.ToString());
            _serialPort.Close();
            _continue = false;
            _serialPort.DataReceived -= new SerialDataReceivedEventHandler(_serialPort_DataReceived);
        }


        public List<string> GetPortNames()
        {
            return SerialPort.GetPortNames().ToList<string>();
        }

    }
}