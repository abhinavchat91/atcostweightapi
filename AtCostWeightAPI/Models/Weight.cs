﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtCostWeightAPI.Models
{
    public class Weight
    {
        public bool Success { get; set; }
        public double Value { get; set; }
        public string[] names { get; set; }
        public Error _errorMessage { get; set; }
    }

    public class Error
    {
        public string Message { get; set; }

        public Error(string msg)
        {
            this.Message = msg;
        }
    }

    public class Port
    {
        public List<string> Names { get; set; }

        public Port()
        {
            this.Names = new List<string>();
        }
    }
}