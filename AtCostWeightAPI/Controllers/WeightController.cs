﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtCostWeightAPI.Models;
using AtCostWeightAPI.Services;
using System.Web.Http.Cors;
using System.ServiceModel.Web;
//using System.Web.Mvc;
using System.Web.Helpers;
using log4net;
using System.Runtime.Caching;
using System.Configuration;

namespace AtCostWeightAPI.Controllers
{
    [RoutePrefix("api/Weight")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WeightController : ApiController
    {
        private WeightRepository _weightRepository;
        private static log4net.ILog Log { get; set; }
        ILog log = log4net.LogManager.GetLogger(typeof(WeightController));      


        public WeightController()
        {
            this._weightRepository = new WeightRepository();
        } 
        
        [Route("Get")]
        [HttpGet]
        public Weight Get()
        {
            var response = _weightRepository.GetWeight();
            
            return response;
        }

        [Route("ErrorTest")]
        [HttpGet]
        public Weight ErrorTest()
        {
            return _weightRepository.ErrorTest();
        }

        [Route("GetCurrentWeight")]
        [HttpGet]
        public Weight GetWeight()
        {
            return _weightRepository.GetCurrentWeight();
        }

        [Route("GetCurrentWeightTest")]
        [HttpGet]
        public Weight GetWeightTest(string PortName)
        {
            return _weightRepository.GetCurrentWeightTest(PortName);
        }

        [Route("Configure")]
        [HttpGet]
        public bool Configure(string PortName, string ScaleType)
        {
            var memoryCache = MemoryCache.Default;

            if (string.IsNullOrEmpty(ScaleType))
            {
                ScaleType = ConfigurationManager.AppSettings["WEIGHTSCALETYPE"].ToString();
            }

            if (!memoryCache.Contains("portname") && !memoryCache.Contains("scaletype"))
            {
                var expiration = DateTimeOffset.UtcNow.AddYears(999);
                var portname = PortName;
                var scaletype = ScaleType;

                memoryCache.Add("portname", portname, expiration);
                memoryCache.Add("scaletype", scaletype, expiration);
            }

            // Reset memory cache
            if (memoryCache.Get("portname").ToString() != PortName)
            {
                memoryCache.Set("portname", PortName, DateTimeOffset.UtcNow.AddYears(999));
            }

            if (memoryCache.Get("scaletype").ToString() != ScaleType)
            {
                memoryCache.Set("scaletype", ScaleType, DateTimeOffset.UtcNow.AddYears(999));
            }

            return true;
        }

    }
}
