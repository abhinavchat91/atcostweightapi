﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//using System.Web.Mvc;
using AtCostWeightAPI.Models;
using System.Web.Http.Cors;
using System.Net.Sockets;
using System.Web.Http.Results;
using System.Net.NetworkInformation;

namespace AtCostWeightAPI.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        // GET api/values/GetMac
        public JObject GetMac()
        {
            string MacAdd = "";
            string IpAdd = "";
            JArray Obj = new JArray();
            dynamic jsonEthernetObject = new JObject();
            dynamic jsonWifiObject = new JObject();
           
            var sys = Dns.GetHostName().ToString();
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in adapters)

            {
                if (adapter.NetworkInterfaceType.ToString().ToLower() == "ethernet" && adapter.OperationalStatus.ToString().ToLower() == "up" && !adapter.Description.Contains("Virtual") && !adapter.Description.Contains("Pseudo"))
                {
                    MacAdd = adapter.GetPhysicalAddress().ToString();
                    IPInterfaceProperties adapterProperties = adapter.GetIPProperties();
                    //IPAddressCollection addr = adapterProperties.DhcpServerAddresses;
                    
                    //if (addr.Count > 0)
                    //{
                    //    foreach (IPAddress address in addr)

                    //    {

                    //        IpAdd = address.ToString();

                    //    }


                    //}
                    //else
                    //{
                        foreach (IPAddressInformation address in adapterProperties.UnicastAddresses)
                        {
                            // We're only interested in IPv4 addresses for now
                            if (address.Address.AddressFamily != AddressFamily.InterNetwork)
                                continue;

                            // Ignore loopback addresses (e.g., 127.0.0.1)
                            if (IPAddress.IsLoopback(address.Address))
                                continue;
                            IpAdd = address.Address.ToString();
                            // sb.AppendLine(address.Address.ToString() + " (" + network.Name + ")");
                        }
                    //}
                    jsonEthernetObject.Name = adapter.Name.ToString();
                    jsonEthernetObject.SystemName = sys;
                    jsonEthernetObject.Status = adapter.OperationalStatus.ToString();
                    jsonEthernetObject.MacAddress = MacAdd;
                    jsonEthernetObject.IPAddress = IpAdd;
                    jsonEthernetObject.NetworkType = adapter.NetworkInterfaceType.ToString().ToLower();
                    //.Add(new JProperty("Name", adapter.Name.ToString())) = new { Name = , Status = , MacAddress = MacAdd, IPAddress = IpAdd, NetworkType =  };
                }
                else if (adapter.NetworkInterfaceType.ToString().ToLower() == "wireless80211" && adapter.OperationalStatus.ToString().ToLower() == "up")
                {
                    MacAdd = adapter.GetPhysicalAddress().ToString();
                    IPInterfaceProperties adapterProperties = adapter.GetIPProperties();
                    var addr = adapterProperties.UnicastAddresses.Select(r => r.Address).ToArray();


                    foreach (IPAddress address in addr)

                    {

                        IpAdd = address.ToString();

                    }


                    jsonWifiObject.Name = adapter.Name.ToString();
                    jsonWifiObject.SystemName = sys;
                    jsonWifiObject.Status = adapter.OperationalStatus.ToString();
                    jsonWifiObject.MacAddress = MacAdd;
                    jsonWifiObject.IPAddress = IpAdd;
                    jsonWifiObject.NetworkType = adapter.NetworkInterfaceType.ToString().ToLower();
                }




            }
            
            
            

            if (((JObject)jsonEthernetObject).Count != 0)
            {
                Obj.Add(jsonEthernetObject);
                
            }

            
            if (((JObject)jsonWifiObject).Count != 0)
            {
                Obj.Add(jsonWifiObject);
            }
            var stringObj = "{'data':"+ Obj.ToString() + "}";
            JObject json = JObject.Parse(stringObj);

            return json;
        }
    }
}
