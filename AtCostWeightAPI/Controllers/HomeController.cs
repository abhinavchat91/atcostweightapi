﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtCostWeightAPI.Models;
using AtCostWeightAPI.Services;
using System.Web.Http.Cors;
using System.ServiceModel.Web;
//using System.Web.Mvc;
using System.Web.Helpers;
using log4net;

namespace AtCostWeightAPI.Controllers
{
    public class HomeController : Controller
    {
        private WeightRepository _weightRepository;
        private static log4net.ILog Log { get; set; }
        ILog log = log4net.LogManager.GetLogger(typeof(HomeController));


        public HomeController()
        {
            this._weightRepository = new WeightRepository();
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            var model = new Port();
            model.Names = _weightRepository.GetPortNames();
            if(model.Names.Count() > 0)
                log.InfoFormat("Portnames are: {0}", model.Names[0]);

            return View(model);
        }
    }
}
